package liskovV01.java.VersionLSP1;

public abstract class BaseWithTabActivity extends BaseActivity {

    /*
                - Burada bütün BaseWithTabActivity'nin Child Activity'leri, kullanması gerekli işlevi, abstract olarak eklenmiştir.
                  Bu sayede bu abstraction'a tüm ekranlar uymak zorundadır.

                  Dolayısıyla da 'BaseWithTabActivity' kullanan tüm işlevler (functions etc..) için
                  hem Parent Child activity'ler sorunsuz olarak çalışmaya devam edecektir.
                  Bu da LSP'ye uygun bir yapı vermiş olacaktır.
     */

    public abstract void menuBottomTab();

    @Override
    public void onCreate(String savedInstanceState) {

        super.onCreate(savedInstanceState);

        // .. default olarak, BOTTOM TAB eklenir.
        menuBottomTab();
    }

    @Override
    protected void onResume() {

        super.onResume();
    }
}
