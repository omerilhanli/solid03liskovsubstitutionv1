package liskovV01.java.VersionLSP2;

import liskovV01.java.VersionLSP2.callback.IMenu;
import liskovV01.java.VersionLSP2.engine.Applier;
import liskovV01.supportlib.Activity;
import liskovV01.supportlib.Toast;

public abstract class BaseActivity extends Activity implements IMenu {

    /*
                - Burada bütün Child Activity'lerin kullanması gerekli işlev IMenu interface'ine eklenmiştir.
                  Bu sayede bu interface(kontrat)'e tüm ekranlar uymak zorundadır.

                  Dolayısıyla da 'BaseActivity' veya 'IMenu' interface'i kullanan işlevler (functions etc..) için
                  hem Parent Child activity'ler sorunsuz olarak çalışmaya devam edecektir.

                  Ancak BaseActivity'i kullanan işlevler sadece, IMenu'i BaseActivity üzerinden kullananlar için kullanabilicektir.

                  Bu da LSP'ye uygun bir yapı vermiş olacaktır.
     */

    @Override
    public void onCreate(String savedInstanceState) {

        super.onCreate(savedInstanceState);

        // .. default olarak, MENU eklenir.
        Applier.apply(this);
        /* Menu ekleme işlevi tüm BaseActivity child'ları için aktif olarak kullanılabilir. */
    }

    @Override
    protected void onResume() {

        super.onResume();

        /* Activity'den türeyen tüm acitivty'ler aynı işlevselliği uyumlu bir şekilde çalıştıracaktır. */
        Toast.makeText(this, "BaseActivity - This is a onResume", Toast.LENGTH_SHORT);
    }


}
