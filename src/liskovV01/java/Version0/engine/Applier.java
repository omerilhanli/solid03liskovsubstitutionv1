package liskovV01.java.Version0.engine;

import liskovV01.java.Version0.BaseActivity;

public class Applier {

    // BaseActivity'ye ait tüm Child Activity'leri için uygulanabilir.
    public static void apply(BaseActivity mBase) {

        mBase.addMenuImplementation();

        mBase.addBottomTabImplementation();
    }

}
