package liskovV01.java.Version0;

import liskovV01.java.Version0.engine.Applier;
import liskovV01.supportlib.Activity;

public abstract class BaseActivity extends Activity {

    public abstract void addMenuImplementation();

    public abstract void addBottomTabImplementation();

    @Override
    public void onCreate(String savedInstanceState) {

        super.onCreate(savedInstanceState);

        // .. default olarak, MENU ve BOTTOM TAB'lar eklenir.
        Applier.apply(this);
        /*
                - Burada iki abstract metod uygulanır. Default olarak tüm Sub-Activity'ler için uygulanan bu metodlardan
                  implementasyonu eklenmemiş olan işlevsiz olarak çalışmak zorunda kalacak ve bu da LSP'ye aykırıdır.
        */
    }

    @Override
    protected void onResume() {

        super.onResume();
    }
}
