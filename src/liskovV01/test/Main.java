package liskovV01.test;

public class Main {

    public static void main(String[] args) {

        // Eğer Interface implement edilirse, Concrete olmayan bir class oluşturulmuş olucaktır.
        Interfaze program = new ProgramConcrete();

        program.zxy();

        // Abstract olarak yazılan class'lardan, anonymous olarak implement edilmeden instance'lar üretilemezler.
        ProgramAbstract programAbstract = new ProgramAbstract() {
            @Override
            public void zxy() {

            }
        };
    }
}
