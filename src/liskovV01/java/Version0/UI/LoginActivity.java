package liskovV01.java.Version0.UI;

import liskovV01.java.Version0.BaseActivity;
import liskovV01.supportlib.Intent;
import liskovV01.supportlib.Log;

public class LoginActivity extends BaseActivity {

    @Override
    public void onCreate(String savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {

        super.onResume();

        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void addMenuImplementation() {
        Log.e(tag, "Ekranında MENU kullanılabilir.");
    }

    /*
          - Bu Ekran aslında sadece bu metod; LSP'yi bozuyor.

            * Çünkü burada alttaki BOTTOM TAB'lar kullanılamaz.
              Ancak bu implementation'lar boş bırakılarak, BOTTOM TAB'lar işlevsiz hale getirilir.
              Bu da LSP Violation'a örnektir.

            * Farklı bi case için, success-implementation sağlamayan class'da Exception
              fırlatılabilir. Bu da uygulama akışını doğru çalışmaz hale getirecektir.
   */
    @Override
    public void addBottomTabImplementation() {

        // EMPTY !!!
    }
}
