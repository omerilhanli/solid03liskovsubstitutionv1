package liskovV01.java.Version0.UI;

import liskovV01.java.Version0.BaseActivity;
import liskovV01.supportlib.Log;

public class ProfileActivity extends BaseActivity {

    @Override
    public void onCreate(String savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    public void addMenuImplementation() {

        Log.e(tag, "Ekranında MENU kullanılabilir.");
    }

    @Override
    public void addBottomTabImplementation() {

        Log.e(tag, "Ekranında BOTTOM TAB'lar kullanılabilir.");
    }
}
