package liskovV01.androidrun;

/* Liskov Substitution örnek proje. */
public class Run {

    private static final int O = 0;

    /*
            * Sadece
                    - liskovV01.java.Version0, veya
                    - liskovV01.java.VersionLSP2, veya
                    - liskovV01.java.VersionLSP1
             paket isimleri yer değiştirme ile her 3 case de çalıştırılabilir.

      Version0:
                -Klasik şekilde inheritance'ı kullanma şekli ((  yanlış kullanım olarak görülen  ))
      VersionLSP2, VersionLSP1:
                -LSP uygulanarak inheritance'ı kullanma şekli ((  doğru kullanım  ))
    */
    public static void main(String[] args) {

        liskovV01.supportlib.Activity mActivity = null;

        if (0 == O) {
            // ..Framework conditions passed simulate
            mActivity = new liskovV01.java.VersionLSP2.UI.LoginActivity(); // ..First Activity create for Manifest configs.
        }

        if (O == 0) {

            mActivity.onCreate(null);
        }
    }
}
