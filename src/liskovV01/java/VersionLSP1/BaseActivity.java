package liskovV01.java.VersionLSP1;

import liskovV01.java.VersionLSP1.engine.Applier;
import liskovV01.supportlib.Activity;

public abstract class BaseActivity extends Activity {

    /*
                - Burada bütün Child Activity'lerin kullanması gerekli işlev abstract olarak eklenmiştir.
                  Bu sayede bu abstraction'a tüm ekranlar uymak zorundadır.

                  Dolayısıyla da 'BaseActivity' kullanan işlevler (functions etc..) için
                  hem Parent Child activity'ler sorunsuz olarak çalışmaya devam edecektir.
                  Bu da LSP'ye uygun bir yapı vermiş olacaktır.
     */

    public abstract void menuLeft();

    @Override
    public void onCreate(String savedInstanceState) {

        super.onCreate(savedInstanceState);

        // .. default olarak, MENU eklenir.
        Applier.apply(this);
        /* Menu ekleme işlevi tüm BaseActivity child'ları için aktif olarak kullanılabilir. */
    }

    @Override
    protected void onResume() {

        super.onResume();
    }
}
