package liskovV01.java.VersionLSP1.engine;

import liskovV01.java.VersionLSP1.BaseActivity;

public class Applier {

    // BaseActivity'ye ait tüm Child Activity'leri için uygulanabilir.
    /*
            - Bu metod bize, LSP uyarınca BaseActivity'den türeyen tüm activity'ler için geçerli bir işlev sağlar.

     */
    public static void apply(BaseActivity activity) {

        activity.menuLeft();
    }

}
