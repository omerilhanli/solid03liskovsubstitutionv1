package liskovV01.supportlib;

public class Log { // ..Log to console

    public static void d(String tag, String message) {

        System.out.println("-Debug- "+tag+": " + message);
    }

    public static void e(String tag, String message) {

        System.out.println("-Error- "+tag+": " + message);
    }

    public static void i(String tag, String message) {

        System.out.println("-Info- "+tag+": " + message);
    }


    public static void w(String tag, String message) {

        System.out.println("-Warning- "+tag+": " + message);
    }
}
