package liskovV01.supportlib;

public class Intent {

    private Context context;
    private Class<?> klass;

    public Intent() {
    }

    public Intent(Context packageContext, Class<?> klass) {
        this.context = packageContext;
        this.klass = klass;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Class<?> getKlass() {
        return klass;
    }

    public void setKlass(Class<?> klass) {
        this.klass = klass;
    }
}
