package liskovV01.java.VersionLSP2.UI;

import liskovV01.java.VersionLSP2.BaseWithTabActivity;
import liskovV01.supportlib.Intent;
import liskovV01.supportlib.Log;

public class MainActivity extends BaseWithTabActivity {

    @Override
    public void onCreate(String savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {

        super.onResume();

        startActivity(new Intent(this, ProfileActivity.class));
    }

    @Override
    public void menuLeft() {

        Log.w(tag, "Ekranında MENU kullanılabilir.");
    }

    @Override
    public void menuBottomTab() {

        Log.w(tag, "Ekranında BOTTOM TAB'lar kullanılabilir.");
    }
}
