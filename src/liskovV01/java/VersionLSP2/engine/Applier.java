package liskovV01.java.VersionLSP2.engine;

import liskovV01.java.VersionLSP2.callback.IMenu;

public class Applier {

    // BaseActivity'ye ait tüm Child Activity'leri için uygulanabilir.
    public static void apply(IMenu menu) {

        menu.menuLeft();
    }

}
