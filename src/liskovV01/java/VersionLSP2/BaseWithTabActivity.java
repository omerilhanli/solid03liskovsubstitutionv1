package liskovV01.java.VersionLSP2;

import liskovV01.java.VersionLSP2.callback.IMenuTab;
import liskovV01.java.VersionLSP2.engine.Applier;
import liskovV01.supportlib.Activity;
import liskovV01.supportlib.Toast;

public abstract class BaseWithTabActivity extends Activity implements IMenuTab {

    /*
                - Burada bütün BaseWithTabActivity'nin Child Activity'leri, kullanması gerekli işlevi, IMenuTab interface'ine eklenmiştir.
                  Bu sayede bu interface'e tüm ekranlar uymak zorundadır.

                  Dolayısıyla da 'IMenu' interface'i kullanan
                  işlevler (functions etc..) için hem Parent hem de Child activity'ler ile
                  sorunsuz olarak çalışmaya devam edecektir ve bu da LSP'ye uygun bir yapı vermiş olacaktır.
     */

    @Override
    public void onCreate(String savedInstanceState) {

        super.onCreate(savedInstanceState);

        // .. default olarak, MENU eklenir.
        Applier.apply(this);
        /* Menu ekleme işlevi tüm BaseWithTabActivity child'ları için aktif olarak kullanılabilir. */


        // .. default olarak, BOTTOM TAB eklenir.
        menuBottomTab();
    }

    @Override
    protected void onResume() {

        super.onResume();

        /* Activity'den türeyen tüm acitivty'ler aynı işlevselliği uyumlu bir şekilde çalıştıracaktır. */
        Toast.makeText(this, "BaseWithTabActivity - This is a onResume", Toast.LENGTH_SHORT);
    }
}
