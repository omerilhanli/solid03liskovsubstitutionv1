package liskovV01.supportlib;

public class Context { // Root context simulate.

    protected final String tag = this.getClass().getPackage().getName() + "." + getClass().getSimpleName();

    protected void onCreate(String savedInstanceState) {

        // ..draw Version0 simulate.

        onResume(); // Activity running
    }

    protected void onResume(){

        // ..Activity running stage
    }

    protected void startActivity(Intent intent) {

        createAndStartActivity(intent); // startActivityForResult simulate,
    }

    private void createAndStartActivity(Intent intent) {

        Context packageContext = intent.getContext();

        Class<?> klass = intent.getKlass();

        try {

            Activity activity = (Activity) Class
                    .forName(packageContext.getClass().getPackage().getName() + "." + klass.getSimpleName()).getConstructor().newInstance();

            activity.onCreate(null);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
