package liskovV01.java.VersionLSP1.UI;

import liskovV01.java.VersionLSP1.BaseActivity;
import liskovV01.supportlib.Intent;
import liskovV01.supportlib.Log;

public class LoginActivity extends BaseActivity {

    @Override
    public void onCreate(String savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {

        super.onResume();

        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void menuLeft() {

        Log.d(tag, "Ekranında MENU kullanılabilir.");
    }
}
