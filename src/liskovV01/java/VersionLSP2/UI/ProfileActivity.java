package liskovV01.java.VersionLSP2.UI;

import liskovV01.java.VersionLSP2.BaseWithTabActivity;
import liskovV01.supportlib.Log;

public class ProfileActivity extends BaseWithTabActivity {

    @Override
    public void onCreate(String savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    public void menuLeft() {

        Log.i(tag, "Ekranında MENU kullanılabilir.");
    }

    @Override
    public void menuBottomTab() {

        Log.i(tag, "Ekranında BOTTOM TAB'lar kullanılabilir.");
    }
}
